import os
import logging
from tqdm import trange

# loglevel retardness.
LOGLEVEL = 30 - int(os.environ["NWDATA_LOGLEVEL"]) * 10
logger = logging.getLogger("NWDATA")
logger.setLevel(LOGLEVEL)
handler = logging.StreamHandler()
formatStr = "[%(levelname)s:%(filename)s:%(funcName)s:%(lineno)d] %(message)s"
formatter = logging.Formatter(formatStr)
handler.setFormatter(formatter)
logger.addHandler(handler)

def drange(*args, **kwargs):
	envVar = int(os.environ["NWDATA_TQDM"]) if "NWDATA_TQDM" in os.environ else 1
	Range = trange(*args, **kwargs) if envVar == 1 else range(*args)
	return Range
