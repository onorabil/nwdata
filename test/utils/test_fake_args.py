import numpy as np
from nwdata.utils import FakeArgs

class TestRunningMean:
	def test_FakeArgs_1(self):
		arr = np.random.randn(10, 20)
		args = FakeArgs({"test" : 1, "npArray" : arr})

		assert args.test == 1
		assert np.allclose(args.npArray, arr)