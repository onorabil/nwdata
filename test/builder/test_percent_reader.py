import sys
import numpy as np
from nwdata import PercentReader, StaticBatchedReader
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parents[1]))
from test_reader import DummyDataset
from test_batched_reader import DummyBatchedDataset

class TestPercentReader:
	def test_constructor_1(self):
		reader = PercentReader(DummyDataset(), percent=100)
		assert not reader is None

	def test_constructor_2(self):
		reader = PercentReader(DummyDataset(), percent=100)
		assert not reader is None

	def test_len_1(self):
		reader = PercentReader(DummyDataset(), percent=100)
		assert len(reader.iterate()) == len(DummyDataset().iterate())

	def test_len_2(self):
		reader = PercentReader(DummyDataset(), percent=10)
		assert len(reader.iterate()) == len(DummyDataset().iterate()) // 10
		assert len(reader.iterate()) == 1

	def test_iterateOneEpoch_1(self):
		reader = DummyDataset()
		readerHalf = PercentReader(reader, percent=50)
		assert len(readerHalf.iterate()) == len(reader.iterate()) // 2

		generator = reader.iterateOneEpoch()
		generatorHalf = readerHalf.iterateOneEpoch()
		n = len(generator)
		nHalf = len(generatorHalf)
		assert nHalf == n // 2
		for i in range(n):
			rgb = next(generator)["data"]["rgb"]
			if i < n // 2:
				rgbHalf = next(generatorHalf)["data"]["rgb"]
				assert np.abs(rgb - rgbHalf).sum() < 1e-5

	def test_iterateForever_1(self):
		reader = DummyDataset()
		readerHalf = PercentReader(reader, percent=50)

		generator = reader.iterateForever()
		generatorHalf = readerHalf.iterateForever()

		n = len(generator)
		rgbs = []
		for i in range(2 * n):
			rgb = next(generator)["data"]["rgb"]
			rgbHalf = next(generatorHalf)["data"]["rgb"]
			if i < n:
				rgbs.append(rgb)
			else:
				assert np.abs(rgb - rgbs[i % n]).sum() < 1e-5

			if i >= n // 2:
				assert np.abs(rgbHalf - rgbs[i % (n//2)]).sum() < 1e-5

class TestPercentReaderBatched:
	def test_constructor_1(self):
		reader = PercentReader(DummyBatchedDataset(), percent=25)
		assert not reader is None

	def test_constructor_2(self):
		reader = PercentReader(DummyBatchedDataset(), percent=25)
		assert not reader is None

	def test_len_1(self):
		reader = PercentReader(DummyBatchedDataset(), percent=100)
		assert len(reader.iterate()) == len(DummyBatchedDataset().iterate())

	def test_len_2(self):
		reader = PercentReader(DummyBatchedDataset(), percent=25)
		assert len(reader.iterate()) == len(DummyBatchedDataset().iterate()) // 4
		assert len(reader.iterate()) == 1

	def test_iterateOneEpoch_1(self):
		reader = StaticBatchedReader(DummyBatchedDataset(N=100), 10)
		readerHalf = PercentReader(reader, percent=50)

		generator = reader.iterateOneEpoch()
		generatorHalf = readerHalf.iterateOneEpoch()
		assert len(generator) == 10
		assert len(generatorHalf) == 5
		assert len(generatorHalf) == len(generator) // 2

		n = len(generator)
		for i in range(n):
			rgb = next(generator)["data"]["rgb"]
			if i < n // 2:
				rgbHalf = next(generatorHalf)["data"]["rgb"]
				assert np.abs(rgb - rgbHalf).sum() < 1e-5

	def test_iterateForever_1(self):
		reader = StaticBatchedReader(DummyBatchedDataset(N=100), 10)
		readerHalf = PercentReader(reader, percent=50)

		generator = reader.iterateForever()
		generatorHalf = readerHalf.iterateForever()

		n = len(generator)
		rgbs = []
		for i in range(2 * n):
			rgb = next(generator)["data"]["rgb"]
			rgbHalf = next(generatorHalf)["data"]["rgb"]
			if i < n:
				rgbs.append(rgb)
			else:
				assert np.abs(rgb - rgbs[i % n]).sum() < 1e-5

			if i >= n // 2:
				assert np.abs(rgbHalf - rgbs[i % (n//2)]).sum() < 1e-5

if __name__ == "__main__":
	TestPercentReaderBatched().test_constructor_1()
	# TestPercentReader().test_iterateForever_1()