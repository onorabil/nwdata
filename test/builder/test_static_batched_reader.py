import sys
import os
import numpy as np
from overrides import overrides
from typing import Tuple, List, Any
from nwdata import StaticBatchedReader
from nwdata.utils import getBatchLens
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parents[1]))
from test_batched_reader import DummyBatchedDataset

class TestStaticBatchedReader:
	def test_constructor_1(self):
		reader = StaticBatchedReader(DummyBatchedDataset(), batchSize=1)
		assert not reader is None

	def test_getItem_1(self):
		reader = StaticBatchedReader(DummyBatchedDataset(), batchSize=1)
		g = reader.iterate()
		item = next(g)
		rgb = item["data"]["rgb"]
		batchLens = getBatchLens(reader.batches)
			
		assert rgb.shape[0] == 1
		assert batchLens[0] == 1
		assert np.abs(rgb - reader.baseReader.dataset[0:1]).sum() < 1e-5

	def test_getItem_2(self):
		reader = StaticBatchedReader(DummyBatchedDataset(), batchSize=1)
		g = reader.iterate()
		n = len(g)
		batches = reader.batches
		batchLens = getBatchLens(batches)
		for j in range(100):
			index = batches[j % n]
			batchItem = g[j % n]
			rgb = batchItem["data"]["rgb"]
			assert len(rgb) == batchLens[j % n]
			assert np.abs(rgb - reader.baseReader.dataset[index.start : index.stop]).sum() < 1e-5

	def test_iterateForever_1(self):
		reader = StaticBatchedReader(DummyBatchedDataset(), batchSize=1)
		g = reader.iterateForever()
		batches = reader.batches
		n = len(batches)
		for j, batchItem in enumerate(g):
			rgb = batchItem["data"]["rgb"]
			try:
				assert len(rgb) == getBatchLens(batches)[j % n]
			except Exception:
				pass
			index = batches[j % n]
			assert np.abs(rgb - reader.baseReader.dataset[index.start : index.stop]).sum() < 1e-5

			if j == 100:
				break

	def test_getNumIterations_1(self):
		reader = DummyBatchedDataset()
		reader1 = StaticBatchedReader(reader, batchSize=1)
		reader2 = StaticBatchedReader(reader, batchSize=2)
		g1 = reader1.iterate()
		g2 = reader2.iterate()
		N1 = len(g1)
		N2 = len(g2)

		assert N1 // 2 == N2

	def test_iterateOneEpoch(self):
		baseReader = DummyBatchedDataset()
		reader1 = StaticBatchedReader(baseReader, batchSize=1)
		reader2 = StaticBatchedReader(baseReader, batchSize=2)
		g1 = reader1.iterate()
		g2 = reader2.iterate()
		n1 = len(g1)
		n2 = len(g2)
		for i in range(n1):
			i1_0 = next(g1)
			i1_1 = next(g1)
			i2 = next(g2)
			rgb1_0 = i1_0["data"]["rgb"]
			rgb1_1 = i1_1["data"]["rgb"]
			rgb2 = i2["data"]["rgb"]

			assert len(rgb1_0) == 1
			assert len(rgb1_1) == 1
			assert len(rgb2) == 2
			assert np.abs(rgb2[0] - rgb1_0[0]).sum() < 1e-5
			assert np.abs(rgb2[1] - rgb1_1[0]).sum() < 1e-5

def main():
	# TestStaticBatchedReader().test_constructor_1()
	TestStaticBatchedReader().test_getNumIterations_1()

if __name__ == "__main__":
	main()