import sys
import os
import numpy as np
from nwdata import CombinedReader
from nwdata.utils import deepCheckEqual
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parents[1]))
from test_reader import DummyDataset
from test_batched_reader import DummyBatchedDataset

class TestCombinedReader:
	def test_constructor_1(self):
		reader = CombinedReader([DummyDataset(), DummyDataset()])
		assert not reader is None

	def test_iterate_constructor_1(self):
		reader1 = DummyDataset()
		reader2 = DummyDataset(N=20)
		reader = CombinedReader([reader1, reader2])

		i1 = reader1.iterate()
		i2 = reader2.iterate()
		i = reader.iterate()
		assert not i1 is None
		assert not i2 is None
		assert not i is None

	def test_len_1(self):
		reader1 = DummyDataset()
		reader2 = DummyDataset(N=20)
		reader = CombinedReader([reader1, reader2])

		g1 = reader1.iterate()
		g2 = reader2.iterate()
		g = reader.iterate()
		assert len(g) == len(g1) + len(g2)

	def test_iterate_1(self):
		reader1 = DummyDataset()
		reader2 = DummyDataset(N=20)
		reader = CombinedReader([reader1, reader2])

		g1 = reader1.iterate()
		g2 = reader2.iterate()
		g = reader.iterate()
		for i in range(len(g)):
			item = next(g)
			if i < len(g1):
				itemOther = next(g1)
			else:
				itemOther = next(g2)
			assert deepCheckEqual(item, itemOther)

class TestCombinedBatchedDataset:
	def test_constructor_1(self):
		reader = CombinedReader([DummyBatchedDataset(), DummyBatchedDataset()])
		assert not reader is None

	def test_iterate_constructor_1(self):
		reader1 = DummyBatchedDataset()
		reader2 = DummyBatchedDataset(N=20)
		reader = CombinedReader([reader1, reader2])

		i1 = reader1.iterate()
		i2 = reader2.iterate()
		i = reader.iterate()
		assert not i1 is None
		assert not i2 is None
		assert not i is None

	def test_len_1(self):
		reader1 = DummyBatchedDataset()
		reader2 = DummyBatchedDataset(N=20)
		reader = CombinedReader([reader1, reader2])

		g1 = reader1.iterate()
		g2 = reader2.iterate()
		g = reader.iterate()
		assert len(g) == len(g1) + len(g2)

	def test_iterate_1(self):
		reader1 = DummyBatchedDataset()
		reader2 = DummyBatchedDataset(N=20)
		reader = CombinedReader([reader1, reader2])

		g1 = reader1.iterate()
		g2 = reader2.iterate()
		g = reader.iterate()
		for i in range(len(g)):
			item = next(g)
			if i < len(g1):
				itemOther = next(g1)
			else:
				itemOther = next(g2)
			assert deepCheckEqual(item, itemOther)

if __name__ == "__main__":
	TestCombinedReader().test_iterate_1()
